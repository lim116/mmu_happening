/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.8-MariaDB : Database - mmu_happening_yuan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mmu_happening` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `mmu_happening`;

/*Table structure for table `tbl_event` */

DROP TABLE IF EXISTS `tbl_event`;

CREATE TABLE `tbl_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `venue` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `type` varchar(50) NOT NULL,
  `event_image` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'P',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `tbl_event_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_event` */

insert  into `tbl_event`(`id`,`title`,`description`,`venue`,`start_date`,`end_date`,`start_time`,`end_time`,`type`,`event_image`,`status`,`created_by`,`created_at`) values 
(1,'test','test','test','2020-02-25','2020-02-25','03:00:00','04:00:00','music','/uploads/2_event_image.jpeg','P',2,'2020-02-25 01:48:58'),
(2,'test','test','test','2020-02-27','2020-02-28','01:15:00','02:45:00','music','/uploads/2_event_image1.jpeg','A',2,'2020-02-25 02:48:39');

/*Table structure for table `tbl_role` */

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role` (
  `role_keyword` varchar(50) NOT NULL,
  `role_description` varchar(50) NOT NULL,
  PRIMARY KEY (`role_keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_role` */

insert  into `tbl_role`(`role_keyword`,`role_description`) values 
('administrator','Administrator'),
('applicant','Applicant');

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `handphone_number` varchar(11) NOT NULL,
  `role_keyword` varchar(50) NOT NULL DEFAULT 'applicant',
  PRIMARY KEY (`user_id`),
  KEY `role_keyword` (`role_keyword`),
  CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`role_keyword`) REFERENCES `tbl_role` (`role_keyword`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`user_id`,`username`,`password`,`fullname`,`email`,`handphone_number`,`role_keyword`) values 
(1,'admin','password','Administrator','admin@mmu_happnening.com','0123456789','administrator'),
(2,'applicant','password','Applicant','applicant@mmuhappening.com','0123456780','applicant');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
