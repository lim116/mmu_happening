<?php

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('event_model');
        $this->load->model('user_model');
    }

    public function index()
    {
        $data['event_list'] = $this->event_model->get_dashboard_list(array('a.status' => 'A'));

        $data['content'] = $this->load->view('login/home', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect();
    }

    public function login()
    {
        if (is_post()) {
            $this->_login_form();
        }

        $data['content'] = $this->load->view('login/login', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _login_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $user_data = $this->user_model->get_data('user_id, fullname, email, handphone_number, role_keyword', $this->input->post(), TRUE, 'array');

            if (!empty($user_data)) {
                $this->_set_login_session($user_data);

                redirect('dashboard');
            } else {
                $message = array(
                    'type'    => 'danger',
                    'message' => 'Invalid Username or Password.'
                );
                $this->session->set_flashdata('message', $message);
            }
        }
    }

    public function register()
    {
        if (is_post()) {
            $this->_register_form();
        }

        $data['content'] = $this->load->view('login/register', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _register_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|max_length[200]'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|max_length[11]'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|alpha_numeric|max_length[50]|callback_check_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|alpha_dash|max_length[50]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|matches[password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $this->user_model->save($this->input->post(array('fullname', 'email', 'handphone_number', 'username', 'password')));

            $message = array(
                'type'    => 'success',
                'message' => 'Successfully register.'
            );
            $this->session->set_flashdata('message', $message);

            redirect('login/login');
        }
    }

    public function check_username()
    {
        if (!empty($this->user_model->get_data('user_id', array('username' => $this->input->post('username')), TRUE))) {
            $this->form_validation->set_message('check_username', 'The Username has already been taken.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function forgot_password()
    {
        if (is_post()) {
            $this->_forgot_password_form();
        }

        $data['content'] = $this->load->view('login/forgot_password', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _forgot_password_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $user_data = $this->user_model->get_data('user_id, fullname, email', $this->input->post(), TRUE);

            if (!empty($user_data)) {
                $this->load->helper('string');

                $password = random_string();

                $this->user_model->save(array('password' => $password), $user_data->user_id);

                $email_data = array(
                    'to' => $user_data->email,
                    'subject' => 'Forgot Password',
                    'message' => "Hi {$user_data->fullname}, <br />
                                  Your password has been reset. <br />
                                  Here your password to login: {$password} .
                                  Click here to go <a href='{URL}'>MMU Happening</a>."
                );

                str_replace('{URL}', site_url('login/login'), $email_data['message']);
                $this->_send_mail($email_data);

                $message = array(
                    'type'    => 'success',
                    'message' => 'Please check your email address.'
                );
                $this->session->set_flashdata('message', $message);

                redirect('login/login');
            } else {
                $message = array(
                    'type'    => 'danger',
                    'message' => 'Invalid Username.'
                );
                $this->session->set_flashdata('message', $message);
            }
        }
    }

    public function event($type = NULL)
    {
        $where['a.status'] = 'A';

        if(!empty($type)){
            $where['a.type'] = $type;
        }

        $data['event_list'] = $this->event_model->get_dashboard_list($where);

        $data['content'] = $this->load->view('login/event_list', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function event_details($id)
    {
        $data['data'] = $this->event_model->get_event_details($id);

        $data['content'] = $this->load->view('login/event_details', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function what_we_do()
    {
        $data['content'] = $this->load->view('login/what_we_do', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function about_us()
    {
        $data['content'] = $this->load->view('login/about_us', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function contact_us()
    {
        $data['content'] = $this->load->view('login/contact_us', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }
}
