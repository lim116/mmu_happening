<?php

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        if (is_post()) {
            if (isset($_POST['update_profile_btn'])) {
                $result = $this->_update_profile();
            } else if (isset($_POST['change_password_btn'])) {
                $result = $this->_change_password();
            }

            if ($result) {
                $message['type'] = TRUE;
                $message['message'] = isset($_POST['update_profile_btn']) ? 'Successfully Update Profile.' : 'Successfully Change Password.';
                $this->session->set_flashdata('message', $message);

                redirect('profile');
            }
        }

        $data['content'] = $this->load->view('profile/profile', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _change_password()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|required|callback_check_password'
            ),
            array(
                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required|alpha_dash|max_length[50]'
            ),
            array(
                'field' => 'confirmation_password',
                'label' => 'Confirmation Password',
                'rules' => 'trim|required|matches[new_password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            return $this->user_model->save(array('password' => $this->input->post('new_password')), $this->session->userdata('user_id'));
        }
    }

    private function _update_profile()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|max_length[200]'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|max_length[11]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Password',
                'rules' => 'trim|required|callback_check_password'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $this->user_model->save($this->input->post(array('fullname', 'email', 'handphone_number')), $this->session->userdata('user_id'));

            $user_data = $this->user_model->get_data('user_id, fullname, email, handphone_number, role_keyword', array('user_id' => $this->session->userdata('user_id')), TRUE, 'array');
            $this->_set_login_session($user_data);

            return TRUE;
        }
    }

    public function check_password($password)
    {
        $where = array(
            'user_id'  => $this->session->userdata('user_id'),
            'password' => $password
        );

        if (!empty($this->user_model->get_data('user_id', $where, TRUE))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_password', 'Password is incorrect.');
            return FALSE;
        }
    }
}
