<?php

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['content'] = $this->load->view('dashboard/dashboard', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }
}
