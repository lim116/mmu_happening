<?php

class Event extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('event_model');
    }

    public function index()
    {
        $data['content'] = $this->load->view('event/list', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function get_dashboard_list()
    {
        $where = array();
        if (empty($this->input->get('status'))) {
            if ($this->session->userdata('role_keyword') == ROLE_APPLICANT) {
                $where['created_by'] = $this->session->userdata('user_id');
            }
        } else {
            if (!empty($_GET['status'])) {
                $where['a.status'] = $this->input->get('status');
            }

            if (!empty($_GET['created_by'])) {
                $where['a.created_by'] = $this->input->get('created_by');
            }

            $where['a.start_date > '] = date('Y-m-d');
        }

        $data['data']         = $this->event_model->get_event_list($where);
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function get_event_list()
    {
        $where = array();
        if ($this->session->userdata('role_keyword') == ROLE_APPLICANT) {
            $where['created_by'] = $this->session->userdata('user_id');
        }

        $data['data']         = $this->event_model->get_event_list($where);
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function form($id = NULL)
    {
        if (is_post()) {
            $this->_save_form($id);
        }

        if (!empty($id)) {
            $data['data'] = $this->event_model->get_data_by_key('id, title, description, venue, start_date, end_date, start_time, end_time, type, event_image, status', $id, TRUE);
        }

        if (!empty($data['data'])) {
            $data['data']->start_date = change_date_format($data['data']->start_date, 'Y-m-d', 'd-m-Y');
            $data['data']->end_date = change_date_format($data['data']->end_date, 'Y-m-d', 'd-m-Y');
        }

        $data['view'] = FALSE;

        $data['content'] = $this->load->view('event/form', @$data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_form($id)
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required|max_length[100]'
            ),
            array(
                'field' => 'venue',
                'label' => 'Venue',
                'rules' => 'trim|required|max_length[100]'
            ),
            array(
                'field' => 'start_date',
                'label' => 'Start Date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'start_time',
                'label' => 'Start Time',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'end_time',
                'label' => 'End Time',
                'rules' => 'trim|required|callback_check_time'
            ),
            array(
                'field' => 'type',
                'label' => 'Event Type',
                'rules' => 'required'
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required'
            )
        ));

        if (empty($_FILES['event_image']['name']) && empty($id)) {
            $this->form_validation->set_rules('event_image', 'Event Image', 'required');
        }

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();
            $data['start_date'] = change_date_format($data['start_date']);
            $data['end_date'] = change_date_format($data['end_date']);
            $data['status'] = 'P';

            if (!empty($_FILES['event_image']['name'])) {
                if (!empty($id)) {
                    $this->_remove_file($id);
                }
                $data['event_image'] = $this->_get_upload_file();
            }

            $this->event_model->save($data, $id);

            redirect('event');
        }
    }

    public function check_time()
    {
        $start = $this->input->post('start_time');
        $end = $this->input->post('end_time');

        if ($start < $end) {
            return TRUE;
        } else if ($start == $end) {
            $this->form_validation->set_message('check_time', 'End Time is same as Start Time.');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_time', 'End Time is less than Start Time.');
            return FALSE;
        }
    }

    private function _remove_file($id)
    {
        $data = $this->event_model->get_data_by_key('event_image', $id, TRUE);

        if (file_exists(FCPATH . $data->event_image)) {
            unlink(FCPATH . $data->event_image);
        }
    }

    private function _get_upload_file()
    {
        $config = array(
            'allowed_types' => 'jpeg|jpg|png',
            'file_name'     => "{$this->session->userdata('user_id')}_event_image",
        );

        return $this->_upload_file('event_image', $config);
    }

    public function pending_list()
    {
        $data['content'] = $this->load->view('event/pending_list', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function get_pending_list()
    {
        $where = array('status' => 'P');

        $data['data']         = $this->event_model->get_event_list($where);
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function view($id)
    {
        if (is_post()) {
            $this->_admin_action($id);
        }

        $data['data'] = $this->event_model->get_data_by_key('id, title, description, venue, start_date, end_date, start_time, end_time, type, event_image, status', $id, TRUE);
        $data['data']->start_date = change_date_format($data['data']->start_date, 'Y-m-d', 'd-m-Y');
        $data['data']->end_date = change_date_format($data['data']->end_date, 'Y-m-d', 'd-m-Y');

        $data['view'] = TRUE;

        $data['content'] = $this->load->view('event/form', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _admin_action($id)
    {
        $this->event_model->save(array('status' => (isset($_POST['approve_btn']) ? 'A' : 'R')), $id);

        redirect('event/pending_list');
    }

    public function action($status, $id)
    {
        $this->event_model->save(array('status' => $status), $id);

        redirect('event/pending_list');
    }
}
