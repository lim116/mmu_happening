<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="mt-4">Event Form</h1>
                    <hr>
                </div>

                <div class="col-lg-7 pull-left">
                    <form method="post" autocomplete="off" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('title'); ?>">
                                    <label for="title">Title : </label>
                                    <input type="text" class="form-control border-md <?php echo form_has_error('title'); ?>" name="title" id="title" value="<?php echo set_value('title', @$data->title); ?>" />
                                    <?php echo form_error_label('title'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('venue'); ?>">
                                    <label for="venue">Venue : </label>
                                    <input type="text" class="form-control border-md <?php echo form_has_error('venue'); ?>" name="venue" id="venue" value="<?php echo set_value('venue', @$data->venue); ?>" />
                                    <?php echo form_error_label('venue'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('start_date'); ?>">
                                    <label for="start_date">Start Date :</label>
                                    <input type="text" class="form-control border-md <?php echo form_has_error('start_date'); ?>" name="start_date" id="start_date" value="<?php echo set_value('start_date', @$data->start_date); ?>" />
                                    <?php echo form_error_label('start_date'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('end_date'); ?>">
                                    <label for="end_date">End Date :</label>
                                    <input type="text" class="form-control border-md datepicker <?php echo form_has_error('end_date'); ?>" name="end_date" id="end_date" value="<?php echo set_value('end_date', @$data->end_date); ?>" />
                                    <?php echo form_error_label('end_date'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('start_time'); ?>">
                                    <label for="start_time">Start Time :</label>
                                    <input type="time" class="form-control border-md <?php echo form_has_error('start_time'); ?>" name="start_time" id="start_time" value="<?php echo set_value('start_time', @$data->start_time); ?>" step="900" />
                                    <?php echo form_error_label('start_time'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('end_time'); ?>">
                                    <label for="end_time">End Time :</label>
                                    <input type="time" class="form-control border-md <?php echo form_has_error('end_time'); ?>" name="end_time" id="end_time" value="<?php echo set_value('end_time', @$data->end_time); ?>" step="900" />
                                    <?php echo form_error_label('end_time'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('type'); ?>">
                                    <label for="type">Event Type :</label>
                                    <select class="form-control border-md <?php echo form_has_error('type'); ?>" name="type" id="type">
                                        <option value="">Please Select</option>
                                        <option value="music" <?php echo set_selected('type', 'music', @$data->type); ?>>Music</option>
                                        <option value="sports" <?php echo set_selected('type', 'sports', @$data->type); ?>>Sports</option>
                                        <option value="club" <?php echo set_selected('type', 'club', @$data->type); ?>>Club / Association</option>
                                        <option value="food" <?php echo set_selected('type', 'food', @$data->type); ?>>Food</option>
                                    </select>
                                    <?php echo form_error_label('type'); ?>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group <?php echo form_has_error('event_image'); ?>">
                                    <label for="event_image">Event Image :</label>
                                    <input type="file" class="form-control-file border-md <?php echo form_has_error('event_image'); ?>" name="event_image" id="event_image" accept="image/*" />
                                    <?php echo form_error_label('event_image'); ?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group <?php echo form_has_error('description'); ?>">
                                    <label for="description">Description :</label>
                                    <textarea class="form-control border-md <?php echo form_has_error('description'); ?>" name="description" id="description" rows="8"><?php echo set_value('description', @$data->description); ?></textarea>
                                    <?php echo form_error_label('description'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <?php if (empty($data)) : ?>
                                <button type="submit" class="btn btn-primary my_right">Submit</button>
                            <?php else: ?>
                                <button type="button" class="btn btn-primary my_right" onclick="history.back();">Back</button>
                                <?php if($this->session->userdata('role_keyword') == ROLE_ADMIN && $data->status == 'P'): ?>
                                    <button type="submit" class="btn btn-danger my_right" name="reject_btn" style="margin-right: 10px;">Reject</button>
                                    <button type="submit" class="btn btn-success my_right" name="approve_btn" style="margin-right: 10px;">Approve</button>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </form>
                </div>

                <div class="col-lg-5 pull-right">
                    <?php if (!empty($data)) : ?>
                        <img src="<?php echo base_url($data->event_image); ?>" width="500px" />
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </main>
</div>

<script>
    $(document).ready(function() {
        //initialize datepicker
        $("#start_date").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            startDate: new Date(),
        });

        $("#end_date").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            startDate: new Date(),
        });

        $('#start_date').change(function() {
            var input_date = $('#start_date').val().trim();

            if (input_date == '' || input_date == null || input_date == undefined) {

            } else {
                var date = parseInt(input_date.substr(0, 2));
                var month = parseInt(input_date.substr(3, 2));
                var year = parseInt(input_date.substr(6, 4));

                $('#end_date').datepicker('setStartDate', new Date(year, month - 1, date));
            }
        });

        <?php if ($view === TRUE) : ?>
            $('input, select, textarea').prop('disabled', true);
        <?php endif; ?>
    });
</script>