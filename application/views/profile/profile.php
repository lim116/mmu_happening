<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <h1 class="mt-4">My profile</h1>
                    <?php
                        $flash = $this->session->flashdata('message');
                        if (!empty($flash)) :
                    ?>
                        <div class="alert alert-success"><?php echo $flash['message']; ?></div>
                    <?php endif; ?>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-5">
                    <form method="post" autocomplete="off">
                        <?php $data = $this->session->userdata(); ?>
                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('fullname'); ?>">
                                <label for="fullname">Fullname : </label>
                                <input type="text" class="form-control border-md <?php echo form_has_error('fullname'); ?>" name="fullname" id="fullname" value="<?php echo set_value('fullname', $data['fullname']); ?>" />
                                <?php echo form_error_label('fullname'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('email'); ?>">
                                <label for="email">Email : </label>
                                <input type="text" class="form-control border-md <?php echo form_has_error('email'); ?>" name="email" id="email" value="<?php echo set_value('email', $data['email']); ?>" />
                                <?php echo form_error_label('email'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('handphone_number'); ?>">
                                <label for="handphone_number">Handphone Number :</label>
                                <input type="text" class="form-control border-md <?php echo form_has_error('handphone_number'); ?>" name="handphone_number" id="handphone_number" value="<?php echo set_value('handphone_number', $data['handphone_number']); ?>" />
                                <?php echo form_error_label('handphone_number'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('confirm_password'); ?>">
                                <label for="confirm_password">Password :</label>
                                <input type="password" class="form-control border-md <?php echo form_has_error('confirm_password'); ?>" name="confirm_password" id="confirm_password" value="<?php echo set_value('confirm_password'); ?>" />
                                <?php echo form_error_label('confirm_password'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary my_right" name="update_profile_btn">Update Profile</button>
                        </div>
                    </form>
                </div>

                <div class="col-lg-5">
                    <form method="post" autocomplete="off">
                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('old_password'); ?>">
                                <label for="old_password">Old Password : </label>
                                <input type="password" class="form-control border-md <?php echo form_has_error('old_password'); ?>" name="old_password" id="old_password" value="<?php echo set_value('old_password'); ?>" />
                                <?php echo form_error_label('old_password'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('new_password'); ?>">
                                <label for="email">New Password : </label>
                                <input type="password" class="form-control border-md <?php echo form_has_error('new_password'); ?>" name="new_password" id="new_password" value="<?php echo set_value('new_password'); ?>" />
                                <?php echo form_error_label('new_password'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group <?php echo form_has_error('confirmation_password'); ?>">
                                <label for="confirmation_password">Confirmation Password :</label>
                                <input type="password" class="form-control border-md <?php echo form_has_error('confirmation_password'); ?>" name="confirmation_password" id="confirmation_password" value="<?php echo set_value('confirmation_password'); ?>" />
                                <?php echo form_error_label('confirmation_password'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary my_right" name="change_password_btn">Change Password</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </main>
</div>