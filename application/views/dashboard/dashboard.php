<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Dashboard</h1>

            <?php if($this->session->userdata('role_keyword') == ROLE_APPLICANT): ?>
                <div class="card mb-4">
                    <div class="card-header"><i class="fas fa-table mr-1"></i>My On Going Event Table</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="my_event_dtb" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Type</th>
                                        <th>Title</th>
                                        <th>Venue</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="card mb-4">
                <div class="card-header"><i class="fas fa-table mr-1"></i>On Going Event Table</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="other_event_dtb" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Type</th>
                                    <th>Title</th>
                                    <th>Venue</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php $this->load->view('components/html_footer'); ?>
</div>

<script>
    var my_event_dtb, other_event_dtb;
    $(document).ready(function() {
        <?php if($this->session->userdata('role_keyword') == ROLE_APPLICANT): ?>
            my_event_dtb = $('#my_event_dtb').DataTable({
                "ajax"       : {
                    url : '<?php echo site_url('event/get_dashboard_list'); ?>',
                    data : {
                        'status'     : 'A',
                        'created_by' : '<?php echo $this->session->userdata('user_id'); ?>'
                    }
                },
                "columns"    : [
                    {data : null},
                    {data : 'type',       name : 'type'},
                    {data : 'title',      name : 'title'},
                    {data : 'venue',      name : 'venue'},
                    {data : 'status',     name : 'status'},
                    {data : 'fullname',   name : 'fullname'},
                    {data : 'created_at', name : 'created_at'},
                    {data : null}
                ],
                "order"      : [[4, 'asc']],
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable"  : false,
                        "targets"    : 0
                    },

                    {
                        "visible" : <?php echo ($this->session->userdata('role_keyword') == ROLE_ADMIN) ? 'true' : 'false'; ?>,
                        "targets" : 5
                    },

                    {
                        'render': function(data, type, row) {
                            var btn = '';

                            btn += '<a href="<?php echo site_url('event/view'); ?>' + '/' + row['id'] + '" class="btn btn-sm btn-info btn-flat" title="View"><i class="fa fa-eye fa-fw"></i></a>&nbsp;';

                            return btn;
                        },
                        "searchable" : false,
                        "orderable"  : false,
                        "targets"    : 7
                    },
                ]
            });

            my_event_dtb.on('order.dt search.dt', function () {
                my_event_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        <?php endif; ?>

        other_event_dtb = $('#other_event_dtb').DataTable({
            "ajax"       : {
                url : '<?php echo site_url('event/get_dashboard_list'); ?>',
                data : {
                    'status' : 'A'
                }
            },
            "columns"    : [
                {data : null},
                {data : 'type',       name : 'type'},
                {data : 'title',      name : 'title'},
                {data : 'venue',      name : 'venue'},
                {data : 'status',     name : 'status'},
                {data : 'fullname',   name : 'fullname'},
                {data : 'created_at', name : 'created_at'},
                {data : null}
            ],
            "order"      : [[4, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 0
                },

                {
                    "visible" : <?php echo ($this->session->userdata('role_keyword') == ROLE_ADMIN) ? 'true' : 'false'; ?>,
                    "targets" : 5
                },

                {
                    'render': function(data, type, row) {
                        var btn = '';

                        btn += '<a href="<?php echo site_url('event/view'); ?>' + '/' + row['id'] + '" class="btn btn-sm btn-info btn-flat" title="View"><i class="fa fa-eye fa-fw"></i></a>&nbsp;';

                        return btn;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 7
                },
            ]
        });

        other_event_dtb.on('order.dt search.dt', function () {
            other_event_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>