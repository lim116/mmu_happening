<nav class="sb-topnav navbar navbar-expand navbar-light bg-white">
    <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>" style="color: black; font-weight: bold;"><span style="color: mediumblue;">MMU</span> Happening!</a>

    <!-- <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button> -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></form>
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('event/form'); ?>" style="color:blue;"><b>Share your event</b></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
            </div>
        </li>
    </ul>
</nav>

<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-white bg-light text-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Dashboard</div>
                <a class="nav-link" href="<?php echo site_url('dashboard'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <div class="sb-sidenav-menu-heading">My profile</div>
                <a class="nav-link" href="<?php echo site_url('profile'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                    My Profile
                </a>

                <div class="sb-sidenav-menu-heading">Events</div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Events
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link" href="<?php echo site_url('event'); ?>">My Event</a>
                        <a class="nav-link" href="<?php echo site_url('event/form'); ?>">Event Form</a>
                    </nav>
                </div>

                <!-- <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="<?php echo site_url(''); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Tables
                </a> -->
            </div>
        </div>
    </nav>
</div>