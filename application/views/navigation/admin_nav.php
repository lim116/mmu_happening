<nav class="sb-topnav navbar navbar-expand navbar-light bg-white">
    <a class="navbar-brand" href="<?php echo site_url('dashboard'); ?>" style="color: black; font-weight: bold;"><span style="color: mediumblue;">MMU</span> Happening!</a>

    <!-- <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button> -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></form>
    <ul class="navbar-nav ml-auto ml-md-0 text-right">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
            </div>
        </li>
    </ul>
</nav>

<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-white bg-light text-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Dashboard</div>
                <a class="nav-link" href="<?php echo site_url('dashboard'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <div class="sb-sidenav-menu-heading">My Profile</div>
                <a class="nav-link" href="<?php echo site_url('profile'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                    My Profile
                </a>

                <div class="sb-sidenav-menu-heading">Events</div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePosts" aria-expanded="false" aria-controls="collapsePosts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Events
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePosts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo site_url('event/pending_list'); ?>">Pending Events</a>
                        <a class="nav-link" href="<?php echo site_url('event'); ?>">All Events</a>
                    </nav>
                </div>

                <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEvents" aria-expanded="false" aria-controls="collapseEvents">
                    <div class="sb-nav-link-icon"><i class="far fa-list-alt"></i></div>
                    Events
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseEvents" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo site_url(''); ?>">Events History</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRequests" aria-expanded="false" aria-controls="collapseRequests">
                    <div class="sb-nav-link-icon"><i class="far fa-comment-alt"></i></div>
                    Requests
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseRequests" aria-labelledby="headingThree" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo site_url(''); ?>">Pending Requests</a>
                        <a class="nav-link" href="<?php echo site_url(''); ?>">Requests History</a>
                    </nav>
                </div> -->
            </div>
        </div>
    </nav>
</div>