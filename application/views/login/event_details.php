<div class="container mt-4 rounded w-100 p-5">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" onclick="window.history.back();" class="btn btn-outline-primary"><< Back</button> <hr>
        </div>
        <div class="col-lg-6 text-left">
            <h1><u><?php echo $data->title; ?> </u></h1>
        </div>
        <hr>
        <div class="col-lg-6 text-right">
            <h3><small>by <?php echo $data->fullname; ?></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6" style="position:relative">
            <div style="display:inline">
                <p><i class="fas fa-map-marked-alt"></i> <?php echo $data->venue; ?></p>
            </div>
            <p style="position: absolute; right: 0;"> <i class="fas fa-calendar-alt"></i><?php echo "{$data->start_date} - {$data->end_date}"; ?></p>
            <hr>
            <h3 class="my-3">Event Description</h3>
            <p><?php echo $data->description; ?></p>
        </div>
        <div class="col-md-6">
            <img class="img-fluid" src=<?php echo base_url($data->event_image); ?> />
        </div>
    </div>

    <div class="row ">
        <div class="col my-2">
            <h5 class="my-4" style="display:inline;margin-right:1rem;">Contact :</h5>
            <h6 style="display: inline;"><?php echo $data->handphone_number; ?></h6>
        </div>
    </div>

    <div class="row ">
        <div class="col my-2">
            <h5 class="my-4" style="display:inline;margin-right:1rem;">Event Time :</h5>
            <h6 style="display: inline;"><?php echo "{$data->start_time} - {$data->end_time}"; ?></h6>
        </div>
    </div>

    <div class="row ">
        <div class="col my-2">
            <h5 class="my-4" style="display:inline;margin-right:1rem;">Event Type :</h5>
            <h6 style="display: inline;"><?php echo $data->type; ?></h6>
        </div>
    </div>
</div>