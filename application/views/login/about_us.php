<div class="image-aboutus-banner" style="margin-top:55px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">About Us</h1>
                <p class="image-aboutus-para">MMU Happening. A place for events in MMU Cyberjaya</p>
            </div>
        </div>
    </div>
</div>

<div class="aboutus-secktion paddingTB60">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="strong">Who we are and<br>what we do</h1>
                <p class="lead">This is the best place for you to<br>share your event </p>
            </div>
            <div class="col-md-6">
                <p>
                    <strong>MMU Happening!</strong> is a place where you can promote your event to MMU Cyberjaya Students.
                </p>
                <p>Just <a href="<?php echo site_url('login/register'); ?>" style="color:blue;">Register</a> and become an applicant to promote your Events in MMU Happening!</p>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="container team-sektion paddingTB60 ">
    <div class="row">
        <div class="col text-center">
            <h2>Our Team</h2>
            <p>We are students from Diploma in IT MMU Cyberjaya </p>
            <div class="border"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-sm-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top img-fluid fit-image" src="<?php echo base_url('assets/images/goh.jpeg') ?>" /></a>
                <div class="card-body">
                    <h4>Goh Jin Lun</h4>
                    <p class="card-text">Student ID : 1171203600</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top img-fluid fit-image" src="<?php echo base_url('assets/images/nad.jpeg') ?>" /></a>
                <div class="card-body">
                    <h4>Nur Nadiah</h4>
                    <p class="card-text">Student ID : 1171203599</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top img-fluid fit-image" src="<?php echo base_url('assets/images/aaron.jpeg') ?>" /></a>
                <div class="card-body">
                    <h4>Goh Zhong Chie</h4>
                    <p class="card-text">Student ID: 1171203680</p>
                </div>
            </div>
        </div>
    </div>
</div>