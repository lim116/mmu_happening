<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active" style="background-image: url('https://source.unsplash.com/RCAhiGJsUUE/1920x1080')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">MMU Happening</h3>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('https://source.unsplash.com/wfh8dDlNFOk/1920x1080')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">A place for you to share your event</h3>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('https://source.unsplash.com/O7fzqFEfLlo/1920x1080')">
                <div class="carousel-caption d-none d-md-block">
                    <h3 class="display-4">And for you to join event</h3>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<div class="container" style="margin:auto;">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-5">MMU Happening</h1>
            <p class="lead">An Online Bulletin Board for events happening in MMU Cyberjaya</p>
        </div>
    </div>
    <h1 class="my-4">Events Happening
        <small>in MMU Cyberjaya</small>
    </h1>

    <div class="row">
        <?php 
            if(!empty($event_list)):
                foreach($event_list as $row) : 
        ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                        <div class="card h-100">
                            <a href="#">
                                <img class="img-fluid card-img-top w-100 h-100" style="height:300px; width:253px;" src="<?php echo base_url($row->event_image); ?>" />
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="<?php echo site_url("login/event_details/{$row->id}"); ?>" class="link"><?php echo $row->title; ?></a>
                                </h4>
                                <p class="card-text"><?php echo $row->description; ?></p>
                            </div>
                        </div>
                    </div>
        <?php 
                endforeach;
            else:
        ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                No Event...
            </div>
        <?php endif; ?>
    </div>
    <div class="text-right" style="margin:auto;">
        <div class="row m-4 text-right">
            <div class="col">
                <a class="btn btn-primary btn-lg" href="<?php echo site_url('login/event/all'); ?>">View more events >></a>
            </div>
        </div>
    </div>

</div>