<div class="container" style="margin:auto;">
    <div class="row">
        <div class="col-lg-12 text-center">
            <br>
            <h1 class="mt-5">Event List</h1>
            <p class="lead">All the interesting upcoming events that are coming to MMU!</p>
        </div>
    </div>
    <div class="container">
        <h1 class="my-4">Upcoming Events
            <small>in Cyberjaya</small>
        </h1>
        <hr>

        <?php
            if(!empty($event_list)):
                foreach($event_list as $row):
        ?>
                    <div class="row m-5">
                        <div class="col-md-7">
                            <a href="#">
                                <img class="img-fluid rounded mb-3 mb-md-0 fit-image " src="<?php echo base_url($row->event_image); ?>" style="width: 100%; max-height: 400px;" />
                            </a>
                        </div>
                        <div class="col-md-5">
                            <h3><?php echo $row->title; ?></h3>
                            <p><?php echo $row->description; ?></p>
                            <a href="<?php echo site_url("login/event_details/{$row->id}") ?>" class="btn btn-primary">View Event >></a>
                        </div>
                    </div>
        <?php
                endforeach;
            else:
        ?>
            <div class="row m-5">No Event...</div>
        <?php endif; ?>
        <hr>
    </div>
</div>