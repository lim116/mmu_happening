<div class="image-aboutus-banner" style="margin-top:55spx">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">MMU Happening!</h1>
                <p class="image-aboutus-para">A place for events happening in MMU Cyberjaya.</p>
            </div>
        </div>
    </div>
</div>

<section id="contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="mt-5">Contact Us</h1>
                <hr>
                <p class="lead">Contact us if you have any question regarding MMU Happening!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card border-0">
                    <div class="card-body text-center ">
                        <i class="fa fa-phone fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class="text-uppercase mb-5">call us</h4>
                        <p>+6 010 4692 133</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <i class="fa fa-map-marker-alt fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class="text-uppercase mb-5">office loaction</h4>
                        <address>FCI, Multimedia University Cyberjaya </address>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <i class="fa fa-envelope fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class="text-uppercase mb-5">email</h4>
                        <p>1171203600@student.mmu.edu.my (Mr Goh)</p>
                        <p>1171203680@student.mmu.edu.my (Aaron)</p>
                        <p>1171203599@student.mmu.edu.my (Nadiah)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>