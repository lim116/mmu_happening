<div class="container mt-5 mb-5 pt-5 pb-5">
    <div class="row py-5 mt-6 align-items-center">
        <div class="col-md-5 pr-lg-5 mb-5 mb-md-0">
            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" class="img-fluid mb-3 d-none d-md-block">
        </div>

        <div class="col-md-2 col-lg-6 ml-auto">
            <form method="post" autocomplete="off">
                <div class="row">
                    <div class="form-group col-lg-12 <?php echo form_has_error('fullname'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-user text-muted"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('fullname'); ?>" name="fullname" placeholder="Fullname" value="<?php echo set_value('fullname'); ?>" />
                        </div>
                        <?php echo form_error_label('fullname'); ?>
                    </div>

                    <div class="form-group col-lg-12 <?php echo form_has_error('email'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-envelope text-muted"></i>
                                </span>
                            </div>
                            <input type="email" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('email'); ?>" name="email" placeholder="Email Address" value="<?php echo set_value('email'); ?>" />
                        </div>
                        <?php echo form_error_label('email'); ?>
                    </div>

                    <div class="form-group col-lg-12 <?php echo form_has_error('handphone_number'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-phone-square text-muted"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('handphone_number'); ?>" name="handphone_number" placeholder="Handphone Number" value="<?php echo set_value('handphone_number'); ?>" />
                        </div>
                        <?php echo form_error_label('handphone_number'); ?>
                    </div>

                    <div class="form-group col-lg-12 <?php echo form_has_error('username'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-user text-muted"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('username'); ?>" name="username" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                        </div>
                        <?php echo form_error_label('username'); ?>
                    </div>

                    <div class="form-group col-lg-6 <?php echo form_has_error('password'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-lock text-muted"></i>
                                </span>
                            </div>
                            <input type="password" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('password'); ?>" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>" />
                        </div>
                        <?php echo form_error_label('password'); ?>
                    </div>

                    <div class="form-group col-lg-6 <?php echo form_has_error('confirm_password'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-lock text-muted"></i>
                                </span>
                            </div>
                            <input type="password" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('confirm_password'); ?>" name="confirm_password" placeholder="Confirm Password" value="<?php echo set_value('confirm_password'); ?>" />
                        </div>
                        <?php echo form_error_label('confirm_password'); ?>
                    </div>

                    <div class="form-group col-lg-12 mx-auto mb-0">
                        <button type="submit" class="btn btn-primary btn-block py-2">
                            <span class="font-weight-bold">Create your account</span>
                        </button>
                    </div>

                    <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4">
                        <div class="border-bottom w-100 ml-5"></div>
                        <span class="px-2 small text-muted font-weight-bold text-muted">OR</span>
                        <div class="border-bottom w-100 mr-5"></div>
                    </div>

                    <div class="text-center w-100">
                        <p class="text-muted font-weight-bold">Already Registered? <a href="<?php echo site_url('login/login'); ?>" class="text-primary ml-2">Login</a></p>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>