<div class="container mt-5 pt-5 mb-5 pb-5 ">
    <div class="row py-6 mt-5 align-items-center">
        <div class="col-md-5 pr-lg-5 mb-5 mb-md-6">
            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" class="img-fluid mb-3 d-none d-md-block">
        </div>

        <div class="col-md-7 col-lg-6 ml-auto">
            <?php
                $flash = $this->session->flashdata('message');
                if (!empty($flash)) :
            ?>
                <div class="alert alert-<?php echo $flash['type']; ?>"><?php echo $flash['message']; ?></div>
            <?php endif; ?>
            <form method="post" autocomplete="off">
                <div class="row">
                    <label class="col-lg-12 mb-2" for="username">Username : </label>
                    <div class="form-group col-lg-12 <?php echo form_has_error('username'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-user text-muted"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('username'); ?>" name="username" id="username" placeholder="Enter Username" value="<?php echo set_value('password'); ?>" />
                        </div>
                        <?php echo form_error_label('username'); ?>
                    </div>

                    <label class="col-lg-12 mb-2" for="password">Password : </label>
                    <div class="form-group col-lg-12 <?php echo form_has_error('password'); ?>">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white px-4 border-md border-right-0">
                                    <i class="fa fa-lock text-muted"></i>
                                </span>
                            </div>
                            <input type="password" class="form-control bg-white border-left-0 border-md <?php echo form_has_error('password'); ?>" name="password" id="password" placeholder="Enter Password" value="<?php echo set_value('password'); ?>" />
                        </div>
                        <?php echo form_error_label('password'); ?>
                    </div>
                </div>

                <div class="form-group mx-auto mb-0">
                    <button type="submit" class="btn btn-primary btn-block py-2">
                        <span class="font-weight-bold">Login</span>
                    </button>
                </div>

                <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4">
                    <div class="border-bottom w-100 ml-5"></div>
                    <span class="px-2 small text-muted font-weight-bold text-muted">OR</span>
                    <div class="border-bottom w-100 mr-5"></div>
                </div>

                <div class="text-center w-100">
                    <p class="text-muted font-weight-bold">Register an account?<a href="<?php echo site_url('login/register'); ?>" class="text-primary ml-2">Register</a></p>
                </div>

                <div class="text-center w-100">
                    <a href="<?php echo site_url('login/forgot_password'); ?>" class="text-primary font-weight-bold ml-2">Forgot your password?</a></p>
                </div>
            </form>
        </div>
    </div>
</div>