<header class="header">
    <nav class="navbar navbar-expand-lg bg-white static-top fixed-top">
        <div class="container-fluid" style="margin:none;">
            <span class="w-100 d-lg-none d-block"></span>
            <div class="mr-auto">
                <a class="navbar-brand" href="<?php echo site_url(); ?>" style="color:black; font-weight:bold;"><span style="color:MediumBlue;">MMU</span s> Happening!</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="ml-auto ">
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('login/login'); ?>" style="color:blue;">Share your event</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                About
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo site_url('login/what_we_do'); ?>">What We Do</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo site_url('login/contact_us'); ?>">Contact Us</a>
                                <a class="dropdown-item" href="<?php echo site_url('login/about_us'); ?>">About Us</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Explore
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo site_url('login/event/music'); ?>">Music</a>
                                <a class="dropdown-item" href="<?php echo site_url('login/event/sports'); ?>">Sports</a>
                                <a class="dropdown-item" href="<?php echo site_url('login/event/club'); ?>">Club / Association</a>
                                <a class="dropdown-item" href="<?php echo site_url('login/event/food'); ?>">Food</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo site_url('login/event'); ?>" style="text-align: center; color: blue;">See All</a>
                            </div>
                        </li>
                        <li class="nav-item" style="border-left: 0.5px solid black;">
                            <a class="nav-link" href="<?php echo site_url('login/login'); ?>">Log In</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>