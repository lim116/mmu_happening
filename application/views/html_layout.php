<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('components/html_head.php'); ?>

    <?php $url = $this->uri->rsegment(1); ?>

    <body class="<?php echo $url == 'login' ? '' : 'sb-nav-fixed' ?>">
        <?php echo $url == 'login' ? '' : '<div id="layoutSidenav">'; ?>

        <?php
        if ($url == 'login') {
            $this->load->view('login/login_nav');
        } else {
            $nav = ($this->session->userdata('role_keyword') == ROLE_ADMIN) ? 'admin' : 'applicant';

            $this->load->view("navigation/{$nav}_nav");
        }
        ?>

        <?php echo $content; ?>

        <?php if ($url == 'login') : ?>
            <div class="container-fluid text-center">
                <footer id="footer fixed-bottom" class="py-4 bg-dark text-white-50">
                    <small>Website Created by Aaron & Nadia & Jin Lun</small>
                </footer>
            </div>            
        <?php endif; ?>

        <?php echo $url == 'login' ? '' : '</div>'; ?>
    </body>

</html>