<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>MMU Happening</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <!-- Jquery 3.4.1 -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>

    <!-- Template's from original developer -->
    <link href="<?php echo base_url('assets/plugins/dist/css/styles.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/plugins/dist/js/scripts.js') ?>"></script>

    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Datatables -->
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>

    <link href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" rel="stylesheet" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

    <!-- Datepicker -->
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>

    <style>
        .nav-item {
            padding-left: 10px;
            padding-right: 10px;
        }

        .site-heading h3 {
            font-size: 40px;
            margin-bottom: 15px;
            font-weight: 600;

        }

        .carousel-item {
            height: 65vh;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .image-aboutus-banner {
            background: linear-gradient(rgba(0, 0, 0, .7), rgba(0, 0, 0, .7)), url("https://images.pexels.com/photos/673649/pexels-photo-673649.jpeg?w=940&h=650&auto=compress&cs=tinysrgb");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
            color: #fff;
            padding-top: 110px;
            padding-bottom: 110px;
        }

        .image-aboutus-sm-banner {
            background: linear-gradient(rgba(0, 0, 0, .7), rgba(0, 0, 0, .7)), url("https://images.pexels.com/photos/631008/pexels-photo-631008.jpeg?w=940&h=650&auto=compress&cs=tinysrgb");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
            color: #fff;
            padding-top: 30px;
            padding-bottom: 30px;
        }

        .image-aboutus-para {
            color: #fff;
        }

        .fit-image {
            width: 100%;
            object-fit: cover;
            height: 400px;
        }

        .border {
            background: #e1e1e1;
            height: 1px;
            width: 25%;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 45px;
        }

        .paddingTB60 {
            padding-top: 60px;
            padding-bottom: 60px;
        }

        .lg-text {
            font-size: 52px;
            font-weight: 600;
            text-transform: none;
            color: #fff;
        }

        .has-error {
            border-color: rgb(200, 0, 0) !important;
            color: rgb(200, 0, 0);
        }

        .sb-nav-fixed #layoutSidenav #layoutSidenav_nav .sb-sidenav {
            padding-top: 56px;
        }

        .my_right {
            float: right;
        }
    </style>
</head>