<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Website Created by Aaron & Nadia & Jin Lun</div>
        </div>
    </div>
</footer>