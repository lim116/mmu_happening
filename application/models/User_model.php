<?php

class User_model extends MY_Model
{
    public function __construct()
    {
        $this->_table_name = 'tbl_user';
        $this->_primary_key = 'user_id';
        $this->_order_by = 'user_id';
    }
}