<?php

class Event_model extends MY_Model
{
    public function __construct()
    {
        $this->_table_name = 'tbl_event';
        $this->_primary_key = 'id';
        $this->_order_by = 'id';
    }

    public function get_event_list($where)
    {
        $this->db->select('a.id, a.title, a.venue, DATE_FORMAT(a.start_date, "%d-%m-%Y") AS start_date, DATE_FORMAT(a.end_date, "%d-%m-%Y") AS end_date,
            a.start_time, DATE_FORMAT(a.created_at, "%d-%m-%Y %H:%i:%s") AS created_at, a.created_by, b.fullname');
        $this->db->select('CASE
            WHEN a.status = "P" THEN "Pending"
            WHEN a.status = "A" THEN "Approved"
            WHEN a.status = "R" THEN "Rejected"
        END AS status', FALSE);
        $this->db->select('CASE
            WHEN a.type = "music" THEN "Music"
            WHEN a.type = "sports" THEN "Sports"
            WHEN a.type = "club" THEN "Club / Association"
            WHEN a.type = "food" THEN "Food"
        END AS type', FALSE);
        $this->db->from("{$this->_table_name} AS a");
        $this->db->join('tbl_user AS b', 'a.created_by = b.user_id');

        if (!empty($where)) {
            $this->db->where($where);
        }

        return $this->db->get()->result();
    }

    public function get_dashboard_list($where)
    {
        $this->db->select('a.id, a.title, a.venue, DATE_FORMAT(a.start_date, "%d-%m-%Y") AS start_date, DATE_FORMAT(a.end_date, "%d-%m-%Y") AS end_date,
            a.start_time, DATE_FORMAT(a.created_at, "%d-%m-%Y %H:%i:%s") AS created_at, a.description, a.event_image, a.created_by, b.fullname');
        $this->db->select('CASE
            WHEN a.status = "P" THEN "Pending"
            WHEN a.status = "A" THEN "Approved"
            WHEN a.status = "R" THEN "Rejected"
        END AS status', FALSE);
        $this->db->select('CASE
            WHEN a.type = "music" THEN "Music"
            WHEN a.type = "sports" THEN "Sports"
            WHEN a.type = "club" THEN "Club / Association"
            WHEN a.type = "food" THEN "Food"
        END AS type', FALSE);
        $this->db->from("{$this->_table_name} AS a");
        $this->db->join('tbl_user AS b', 'a.created_by = b.user_id');

        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->where(date('Y-m-d') . ' < ', 'a.end_date', FALSE);

        return $this->db->get()->result();
    }

    public function get_event_details($id)
    {
        $this->db->select('a.id, a.title, a.venue, DATE_FORMAT(a.start_date, "%d-%m-%Y") AS start_date, DATE_FORMAT(a.end_date, "%d-%m-%Y") AS end_date,
            a.start_time, a.end_time, DATE_FORMAT(a.created_at, "%d-%m-%Y %H:%i:%s") AS created_at, a.description, a.event_image, a.created_by, b.fullname, b.handphone_number');
        $this->db->select('CASE
            WHEN a.status = "P" THEN "Pending"
            WHEN a.status = "A" THEN "Approved"
            WHEN a.status = "R" THEN "Rejected"
        END AS status', FALSE);
        $this->db->select('CASE
            WHEN a.type = "music" THEN "Music"
            WHEN a.type = "sports" THEN "Sports"
            WHEN a.type = "club" THEN "Club / Association"
            WHEN a.type = "food" THEN "Food"
        END AS type', FALSE);

        $this->db->from("{$this->_table_name} AS a");
        $this->db->join('tbl_user AS b', 'a.created_by = b.user_id');
        $this->db->where('a.id', $id);

        return $this->db->get()->row();
    }
}
