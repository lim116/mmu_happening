<?php

/**
 * @author Lim Chun Yuan <lim116@yahoo.com>
 * @version 1.0.0
 * @since 15-FEB-2020 - 15-FEB-2020
 */

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_check_login();
    }

    private function _check_login(): void
    {
        $url = $this->uri->rsegment(1);

        if (!empty($this->session->userdata('user_id'))) {
            if ($this->uri->segment(2) == 'logout') {
                //do nothing
            } else if ($url == 'login') {
                redirect('dashboard');
            }
        } else {
            if ($url != 'login') {
                redirect();
            }
        }
    }

    protected function _set_login_session($user_data): void
    {
        $user_data['loggedin'] = TRUE;

        $this->session->set_userdata($user_data);
    }

    protected function _send_mail(array $email_data): bool
    {
        $this->load->library('email');

        $this->email->from('mmu_happening@mmuhappening.com', 'MMU Happening');
        $this->email->to($email_data['to']);
        $this->email->cc('lim116@yahoo.com');
        $this->email->subject($email_data['subject']);
        $this->email->message($email_data['message']);

        return $this->email->send();
    }

    protected function _upload_file($file_name, $config = NULL)
    {
        $this->load->library('upload', $config);

        if ($this->upload->do_upload($file_name)) {
            return "/uploads/{$this->upload->data('file_name')}";
        } else {
            log_message('error', $this->upload->display_errors());
        }
    }
}
