<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Lim Chun Yuan <lim116@yahoo.com>
 * @version 1.0.0
 * @since 15-FEB-2020 - 15-FEB-2020
 */

if ( ! function_exists('is_get'))
{
    /**
     * is_get
     *
     * @return boolean
     */
    function is_get(): bool
    {
        return (isset($_GET) && ! empty($_GET));
    }
}

if ( ! function_exists('is_post'))
{
    /**
     * is_post
     *
     * @return boolean
     */
    function is_post(): bool
    {
        return (isset($_POST) && ! empty($_POST));
    }
}

if ( ! function_exists('change_date_format'))
{
    /**
     * change_date_format
     *
     * @return string
     */
    function change_date_format($date, $from_format = 'd-m-Y', $to_format = 'Y-m-d'): string
    {
        $new_date = DateTime::createFromFormat($from_format, $date);
		return $new_date->format($to_format);
    }
}

if ( ! function_exists('form_has_error'))
{
    /**
     * form_has_error
     *
     * @param string $field
     * @return string '' OR 'has-error'
     */
    function form_has_error(string $field): string
    {
        return empty(form_error($field)) ? '' : 'has-error';
    }
}

if ( ! function_exists('form_error_label'))
{
    /**
     * form_error_label
     *
     * @param string $field
     * @return string '' OR '<label class="control-label" for="inputError">' . form_error($field) . '</label>'
     */
    function form_error_label(string $field): string
    {
        return empty(form_error($field)) ? '' : '<label class="control-label" for="inputError">' . form_error($field) . '</label>';
    }
}

if ( ! function_exists('set_checked'))
{
    /**
     * set_checked
     *
     * @param string $field
     * @param mixed $field_value
     * @param mixed $form_value
     * @param string $type
     * @return string '' OR 'checked'
     */
	function set_checked(string $field, $field_value, $form_value = NULL, $type = 'checkbox'): string
	{
        $function_to_call = "set_{$type}";

        return $function_to_call($field, $field_value, $field_value == set_value($field, $form_value));
	}
}

if ( ! function_exists('set_selected'))
{
    /**
     * set_selected
     *
     * @param string $field
     * @param mixed $field_value
     * @param mixed $form_value
     * @param string $type
     * @return string '' OR 'selected'
     */
	function set_selected(string $field, $field_value, $form_value = NULL): string
	{
        return set_select($field, $field_value, $field_value == set_value($field, $form_value));
	}
}

if ( ! function_exists('set_menu_active'))
{
    /**
     * set_menu_active
     *
     * @param string $menu
     * @param integer $segment
     * @return string '' OR 'active'
     */
	function set_menu_active(string $menu, int $segment = 1): string
	{
        $current_url = get_instance()->uri->segment($segment);

        return $current_url == $menu ? 'active' : '';
	}
}