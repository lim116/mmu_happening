<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Lim Chun Yuan <lim116@yahoo.com>
 * @version 1.0.0
 * @since 15-FEB-2020 - 15-FEB-2020
 */

class MY_Email extends CI_Email
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    private $_config = array(
        'useragent'      => 'CodeIgniter',
        'protocol'       => 'smtp',
        'mailpath'       => '/usr/sbin/sendmail',
        'smtp_host'      => 'smtp.gmail.com',
        'smtp_user'      => 'testsmtpde@gmail.com',
        'smtp_pass'      => 'uuzdvhmvlnxzzbbm',
        'smtp_port'      => 587,
        'smtp_timeout'   => 5,
        'smtp_keepalive' => FALSE,
        'smtp_crypto'    => 'tls',
        'wordwrap'       => TRUE,
        'wrapchars'      => 76,
        'mailtype'       => 'html',
        'charset'        => 'utf-8',
        'validate'       => FALSE,
        'priority'       => 5,
        'crlf'           => "\r\n",
        'newline'        => "\r\n",
        'bcc_batch_mode' => FALSE,
        'bcc_batch_size' => 200,
        'dsn'            => FALSE
    );

    public function __construct(array $config = array())
    {
        if ( ! empty($config))
        {
            foreach ($config as $key => $value)
            {
                $this->_config[$key] = $value;
            }
        }

        parent::__construct($this->_config);
    }
}